package com.jn.myapplication.adapters;

import android.os.Parcelable;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.jn.myapplication.model.Tab;

import java.util.ArrayList;

public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Tab> arrayItems;

    public TabsPagerAdapter(FragmentManager fm, ArrayList<Tab> arrayListTabs) {
        super(fm);
        arrayItems = arrayListTabs;
    }



    @Override
    public int getCount() {
        return arrayItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return arrayItems.get(position).getTitle();

    }

    @Override
    public Fragment getItem(int position) {
        return arrayItems.get(position).getFragment_content();
    }


    //FIXME: corregir estados de fragmentos, actualmente los refresca
    @Override
    public Parcelable saveState() {
        return  null;
    }

    //    @Override
//    protected Fragment createItem(int position) {
//        return arrayItems.get(position).getFragment_content();
//    }


}
