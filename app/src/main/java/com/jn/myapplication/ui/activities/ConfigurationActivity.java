package com.jn.myapplication.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.jn.myapplication.R;
import com.jn.myapplication.adapters.TabsPagerAdapter;
import com.jn.myapplication.model.Tab;
import com.jn.myapplication.ui.fragments.CalibrarFragment;
import com.jn.myapplication.ui.fragments.StatusFragment;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfigurationActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    Handler bluetoothIn;
    final int handlerState = 0;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder DataStringIN = new StringBuilder();
    private ConnectedThread MyConexionBT;

    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static String address = null;


    private ArrayList<Tab> arrayTabOption = new ArrayList<>();

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        ButterKnife.bind(this);


        arrayTabOption.clear();
        arrayTabOption.add(new Tab(1, "CALIBRAR", CalibrarFragment.newInstance()));
        arrayTabOption.add(new Tab(2, "STATUS", StatusFragment.newInstance()));

        TabsPagerAdapter pagerAdapter = new TabsPagerAdapter(getSupportFragmentManager(), arrayTabOption);
        viewPager.setAdapter(pagerAdapter);


        tabLayout.setupWithViewPager(viewPager);


        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;
                    Log.i("data", readMessage);
                    DataStringIN.append(readMessage);

                    try {

                        JSONObject response = new JSONObject(readMessage);

                        //((StatusFragment)arrayTabOption.get(1).getFragment_content()).setPesoActual("envio desde main");
                        ((StatusFragment)arrayTabOption.get(1).getFragment_content()).setPercentaje(""+response.getString("progress"));

                    }catch (Exception e) {

                    }

                    int endOfLineIndex = DataStringIN.indexOf("#");

                    if (endOfLineIndex > 0) {
                        String dataInPrint = DataStringIN.substring(0, endOfLineIndex);
                        //txtBuffer.setText("Dato: " + dataInPrint);
                        DataStringIN.delete(0, DataStringIN.length());
                    }
                }
            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter(); // get Bluetooth adapter
        statusBT();


        //((StatusFragment)arrayTabOption.get(1).getFragment_content()).reciboDato("envio desde main");


    }


    public void enviarCalibrar(String value) {
        Toast.makeText(this, ""+value, Toast.LENGTH_SHORT).show();
        if (MyConexionBT != null)
            MyConexionBT.write(value);
    }

    public void enviarPesoMaximo(String peso) {

        Toast.makeText(this, "enviar="+peso, Toast.LENGTH_SHORT).show();
//        if (MyConexionBT != null)
//            MyConexionBT.write(peso);

    }


    public void enviarPesoMinimo(String peso) {

        if (MyConexionBT != null)
            MyConexionBT.write(peso);

    }


    public void iniciarDeposito(String valor) {

        if (MyConexionBT != null)
            MyConexionBT.write(valor);

    }


    public void pausarDeposito(String valor) {

        if (MyConexionBT != null)
            MyConexionBT.write(valor);

    }

    public void detenerDeposito(String valor) {

        if (MyConexionBT != null)
            MyConexionBT.write(valor);

    }



    @OnClick(R.id.test)
    public void test() {
        ((StatusFragment)arrayTabOption.get(1).getFragment_content()).setPesoActual("envio desde main");
        ((StatusFragment)arrayTabOption.get(1).getFragment_content()).setPercentaje("50");
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        //crea un conexion de salida segura para el dispositivo
        //usando el servicio UUID
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }


    @Override
    public void onResume() {
        super.onResume();

        Intent intent = getIntent();

        address = intent.getStringExtra(DeviceBTActivity.EXTRA_DEVICE_ADDRESS);



        try {
            BluetoothDevice device = btAdapter.getRemoteDevice(address);

            try {
                btSocket = createBluetoothSocket(device);
            } catch (IOException e) {
                Toast.makeText(getBaseContext(), "La creacción del Socket fallo", Toast.LENGTH_LONG).show();
            }

            try {
                btSocket.connect();
            } catch (IOException e) {
                try {
                    btSocket.close();
                } catch (IOException e2) {
                }
            }
            MyConexionBT = new ConnectedThread(btSocket);
            MyConexionBT.start();
        }catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            if (btSocket != null)
                btSocket.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }


    private void statusBT() {

        if (btAdapter == null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }


    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;


            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    String readMessage = new String(buffer, 0, bytes);

                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        //Envio de trama
        public void write(String input) {
            try {
                mmOutStream.write(input.getBytes());
            } catch (IOException e) {

                Toast.makeText(getBaseContext(), "La Conexión fallo", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }


}
