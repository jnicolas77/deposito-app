package com.jn.myapplication.ui.fragments;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.jn.myapplication.R;
import com.jn.myapplication.ui.activities.ConfigurationActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import az.plainpie.PieView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StatusFragment extends Fragment {

    private StatusViewModel mViewModel;

    @BindView(R.id.chartPercent)
    PieView chartPercent;

    @BindView(R.id.pesoActual)
    TextView pesoActual;

    public static StatusFragment newInstance() {
        return new StatusFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.status_fragment, container, false);

        ButterKnife.bind(this, view);

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(StatusViewModel.class);



    }


    @OnClick(R.id.btnIniciar)
    public void iniciar() {
        ((ConfigurationActivity) Objects.requireNonNull(getActivity())).iniciarDeposito("start");
    }

    @OnClick(R.id.btnPausar)
    public void pausar() {
        ((ConfigurationActivity) Objects.requireNonNull(getActivity())).pausarDeposito("progress");
    }

    @OnClick(R.id.btnDetener)
    public void detener() {
        ((ConfigurationActivity) Objects.requireNonNull(getActivity())).detenerDeposito("stop");
    }

    public void setPercentaje (String value) {
        chartPercent.setPercentage(Float.parseFloat(value));
    }

    public void setPesoActual(String dato) {
        pesoActual.setText(dato);

    }

}
