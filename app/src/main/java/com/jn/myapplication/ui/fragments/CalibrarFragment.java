package com.jn.myapplication.ui.fragments;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jn.myapplication.R;
import com.jn.myapplication.ui.activities.ConfigurationActivity;
import com.jn.myapplication.utils.view.CustomEditText;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CalibrarFragment extends Fragment {

    private CalibrarViewModel mViewModel;

    @BindView(R.id.txtPesoMinimo)
    CustomEditText txtPesoMinimo;

    @BindView(R.id.txtPesoMaximo)
    CustomEditText txtPesoMaximo;



    public static CalibrarFragment newInstance() {
        return new CalibrarFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.calibrar_fragment, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CalibrarViewModel.class);
        // TODO: Use the ViewModel
    }


    @OnClick(R.id.btnEnviar)
    public void enviar() {

        if (TextUtils.isEmpty(txtPesoMinimo.getText())) {
            Toast.makeText(getActivity(), "Ingrese peso minimo", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(txtPesoMaximo.getText())) {
            Toast.makeText(getActivity(), "Ingrese peso maximo", Toast.LENGTH_SHORT).show();
        } else {
            ((ConfigurationActivity) Objects.requireNonNull(getActivity())).enviarCalibrar("{\"min\": "+txtPesoMinimo.getText().toString()+", \"max\": "+txtPesoMaximo.getText().toString()+", \"tot\": "+20+"}");
        }



    }


}
