package com.jn.myapplication.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.jn.myapplication.R;
import com.rengwuxian.materialedittext.MaterialEditText;

public class CustomEditText extends MaterialEditText {

    public CustomEditText(Context context) {
        super(context);
        setDefaultValues(context, null);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDefaultValues(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
        setDefaultValues(context, attrs);
    }


    private void setDefaultValues (Context context, AttributeSet attrs) {


        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CEditText);


        switch (ta.getInt(R.styleable.CEditText_cEditText,-1)) {
            case 0:
                setFloatingLabel(FLOATING_LABEL_HIGHLIGHT);
                //setBaseColor(context.getResources().getColor(R.color.md_white_1000));
                setPrimaryColor(context.getResources().getColor(R.color.md_black_1000));
                setTextColor(context.getResources().getColor(R.color.md_black_1000));
                setHintTextColor(context.getResources().getColor(R.color.md_grey_600));
                setErrorColor(R.color.md_red_900);
                setHelperTextColor(context.getResources().getColor(R.color.md_black_1000));
                setUnderlineColor(context.getResources().getColor(R.color.md_black_1000));
                setFloatingLabelTextColor(context.getResources().getColor(R.color.md_black_1000));
                setSingleLineEllipsis(true);
                setSingleLine(true);
                break;

            default:
                setFloatingLabel(FLOATING_LABEL_HIGHLIGHT);
                //setBaseColor(context.getResources().getColor(R.color.md_white_1000));
                setPrimaryColor(context.getResources().getColor(R.color.md_black_1000));
                setTextColor(context.getResources().getColor(R.color.md_black_1000));
                setHintTextColor(context.getResources().getColor(R.color.md_grey_600));
                setErrorColor(R.color.md_red_900);
                setHelperTextColor(context.getResources().getColor(R.color.md_black_1000));
                setUnderlineColor(context.getResources().getColor(R.color.md_black_1000));
                setFloatingLabelTextColor(context.getResources().getColor(R.color.md_black_1000));
                setSingleLineEllipsis(true);
                setSingleLine(true);
                break;
        }





    }

}
