package com.jn.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.fragment.app.Fragment;



public class Tab implements Parcelable {


    private int idTab;
    private String title;
    private Fragment fragment_content;

    public Tab(int idTab, String title, Fragment fragment_content) {
        this.idTab = idTab;
        this.title = title;
        this.fragment_content = fragment_content;
    }

    protected Tab(Parcel in) {
        idTab = in.readInt();
        title = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idTab);
        dest.writeString(title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Tab> CREATOR = new Creator<Tab>() {
        @Override
        public Tab createFromParcel(Parcel in) {
            return new Tab(in);
        }

        @Override
        public Tab[] newArray(int size) {
            return new Tab[size];
        }
    };

    public int getIdTab() {
        return idTab;
    }

    public void setIdTab(int idTab) {
        this.idTab = idTab;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Fragment getFragment_content() {
        return fragment_content;
    }

    public void setFragment_content(Fragment fragment_content) {
        this.fragment_content = fragment_content;
    }
}